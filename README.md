# tcpdump

Tcpdump prints the contents of network packets. It can read packets from a network interface card or from a previously created saved packet file. 

//Config::
    Ubuntu
    


1. How to build/make and then run tcpdump from the source
    1. go the directory where this files is located
    2. open terminal and cd [directory in which the this file is]
    2. sudo apt-get install libpcap-dev   // command to install libpcap from the terminal ,Install libpcap
    4. run ./configure (a shell script).  //"configure" will determine your system attributes and generate an appropriate Makefile from Makefile.in.
    5. sudo make 
    6. make install                       // in the terminal execute make install ,This will install tcpdump and the manual entry.
    6. sudo ./tcpdump [args]              // 

2. Alternatively you can install tcpdump through the folowing command in the terminal :
    1. sudo apt-get install tcpdump


Notes:


1. The Folder named “Unsuccessfully Attempts” contains various ways that I tried to create a Sniffing Program. 

    1. Issue: Most of the programs in the folder were able to capture packets in the Managed Mode , bur were not able to do so in the Monitor mode.
    2. Other ways to make a sniffer(Pending) :
	    1. libnl
	    2. sockets

2. Tcpdump usage (cheatsheet)
    1. https://www.sans.org/security-resources/tcpip.pdf	






    
    
    



