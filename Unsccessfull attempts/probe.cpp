// make sure to link against /usr/lib/libpcap.dylib
//#include "ofMain.h"
#include <pcap/pcap.h>

class ofApp : public ofBaseApp {
public:
    pcap_t *pcap;
    struct pcap_pkthdr header;
    const unsigned char* packet = NULL;
    list<string> packetHex;
    
	void setup() {
        char filter[] = "type mgt subtype probe-req";
        char dev[] = "en0";
        struct bpf_program fp;
        char errbuf[PCAP_ERRBUF_SIZE];
        pcap = pcap_create(dev,errbuf);
        pcap_set_rfmon(pcap, 1);
        pcap_set_snaplen(pcap, 2048);
        pcap_set_promisc(pcap, 1);
        pcap_set_timeout(pcap, 512);
        
        int status;
        status = pcap_activate(pcap);
        status = pcap_compile(pcap, &fp, filter, 0, 0);
        status = pcap_setfilter(pcap, &fp);
	}
	void update() {
        packet = pcap_next(pcap, &header);
        if (packet) {
            ostringstream out;
            int n = header.caplen;
            for(int i = 0; i < n; i++) {
                out << setfill('0') << setw(2) << hex << (int) packet[i] << " ";
            }
            packetHex.push_back(out.str());
            if(packetHex.size() > 36) {
                packetHex.pop_front();
            }
        }
	}
	void draw() {
        ofBackground(0);
        list<string>::iterator ptr;
        int y = 0;
        for(ptr = packetHex.begin(); ptr != packetHex.end(); ptr++) {
            ofDrawBitmapString(*ptr, 10, y += 20);
        }
	}
};

int main() {
	ofSetupOpenGL(1280, 720, OF_WINDOW);
	ofRunApp(new ofApp());
}
